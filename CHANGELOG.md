# CHANGELOG

## 1.0.0 ( 2023-06-16 )

* New pipeline script.

## 0.1.0 ( 2022-10-18 )

* [build_agent](./roles/build_agent) role.
